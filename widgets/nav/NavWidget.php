<?php
/**
 * @link http://www.writesdown.com/
 * @copyright Copyright (c) 2015 WritesDown
 * @license http://www.writesdown.com/license/
 */

namespace widgets\nav;

use common\components\BaseWidget;
use common\models\Menu;
use themes\nextgen\classes\widgets\Nav;
use Yii;

/**
 * Class MetaWidget
 *
 * @author Agiel K. Saputra <13nightevil@gmail.com>
 * @since 0.1.1
 */
class NavWidget extends BaseWidget
{
    public $menu='';
    public function run()
    {
        echo $this->beforeWidget;

        if ($this->title) {
            echo $this->beforeTitle . $this->title . $this->afterTitle;
        }
        echo Nav::widget([
                'activateParents' => true,
                'options' => ['class' => ''],
                'items' => Menu::get($this->menu),
                'encodeLabels' => false,
            ]);
        echo $this->afterWidget;
    }
}
