<?php
/**
 * @link http://www.writesdown.com/
 * @copyright Copyright (c) 2015 WritesDown
 * @license http://www.writesdown.com/license/
 */

namespace widgets\postslider;

use common\components\BaseWidget;
use Yii;
use yii\widgets\Menu;

/**
 * Class MetaWidget
 *
 * @author Agiel K. Saputra <13nightevil@gmail.com>
 * @since 0.1.1
 */
class PostsliderWidget extends BaseWidget
{
    /**
     * @inheritdoc
     */
    public function run()
    {
        echo $this->beforeWidget;

        if ($this->title) {
            echo $this->beforeTitle . $this->title . $this->afterTitle;
        }

        echo 'tes';
        echo $this->afterWidget;
    }
}
