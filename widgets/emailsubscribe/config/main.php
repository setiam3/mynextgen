<?php
/**
 * @link http://www.writesdown.com/
 * @author Agiel K. Saputra <13nightevil@gmail.com>
 * @copyright Copyright (c) 2015 WritesDown
 * @license http://www.writesdown.com/license/
 */

return [
    'title' => 'Email Subscribe',
    'config' => [
        'class' => 'widgets\emailsubscribe\EmailsubscribeWidget',
        'title' => '',
    ],
];
