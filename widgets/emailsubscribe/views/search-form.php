<?php
/**
 * @link http://www.writesdown.com/
 * @author Agiel K. Saputra <13nightevil@gmail.com>
 * @copyright Copyright (c) 2015 WritesDown
 * @license http://www.writesdown.com/license/
 */
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;
?>
<p>Get the latest articles from us</p>

<?php $form = ActiveForm::begin([
    'action' => Url::to(['/site/emailsubscribe']),
    'method' => 'POST',
    'options' => [
        'class' => 'search-form',
    ],
]) ?>
<div class="input-group" style="">
<?= Html::textInput('email', Yii::$app->request->get('email'), [
    'class' => 'search-form-field',
    'style'=>'border-radius: 50rem;border: none;padding: 1rem;background: transparent;z-index: 1;position: inherit;',
    'placeholder' => 'Enter Your Email',
]) ?>
<div class="rounded-pill" style="background: #ffe9ab;position: absolute;width: 100%;height: 4rem;opacity: 0.9;z-index: 0;top: 0;"></div>
<span class="icon-inside">
<i class="fa fa-arrow-right rounded-pill" style="padding:4px;background:white"></i>
</span>
</div>
<?php ActiveForm::end() ?>