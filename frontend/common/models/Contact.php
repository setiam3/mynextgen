<?php

namespace app\common\models;

use Yii;

/**
 * This is the model class for table "{{%contact}}".
 *
 * @property int $id
 * @property string $name
 * @property string $email
 * @property string $voice
 * @property string $messages
 * @property string $create_date
 * @property int $status_reply 1 notreplied, 2 replied
 */
class Contact extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%contact}}';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['name', 'email', 'create_date'], 'required'],
            [['messages'], 'string'],
            [['create_date'], 'safe'],
            [['status_reply'], 'integer'],
            [['name', 'email', 'voice'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'email' => 'Email',
            'voice' => 'Voice',
            'messages' => 'Messages',
            'create_date' => 'Create Date',
            'status_reply' => 'Status Reply',
        ];
    }
}
