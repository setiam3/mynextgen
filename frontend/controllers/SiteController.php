<?php
/**
 * @link http://www.writesdown.com/
 * @copyright Copyright (c) 2015 WritesDown
 * @license http://www.writesdown.com/license/
 */
namespace frontend\controllers;
use common\models\LoginForm;
use common\models\User;
use common\models\Option;
use common\models\Post;
use common\models\PostComment;
use common\models\Contact;
use frontend\models\ContactForm;
use Yii;
use yii\data\Pagination;
use yii\filters\AccessControl;
use yii\filters\VerbFilter;
use yii\web\Controller;
use yii\web\ForbiddenHttpException;
use yii\web\NotFoundHttpException;
use yii\web\UploadedFile;
use yii\helpers\FileHelper;
use yii\imagine\Image;
/**
 * Class SiteController
 *
 * @author Agiel K. Saputra <13nightevil@gmail.com>
 * @since 0.1.0
 */
class SiteController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout'],
                'rules' => [
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }
    public function actionEmailsubscribe(){
        print_r($_POST['email']);
    }
    public function actionLogin(){
        $model=new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            return $this->goBack();
        }
        return $this->render('login',['model'=>$model]);
    }
    public function actionRegister(){
        $model=new LoginForm();
        return $this->render('register',['model'=>$model]);
    }
    public function actionThanksjoin(){
        return $this->render('thanksjoin');
    }
    public function actionProfile(){
        if(!empty(Yii::$app->user->getId())){
            $model=User::findOne(Yii::$app->user->getId());
        }
        if($model->load(Yii::$app->request->post())){
            $ar=explode("\\",get_class($model));
            if(!empty(Yii::$app->request->post(end($ar))['password'])){
                $model->setPassword(Yii::$app->request->post(end($ar))['password']);
            }
            if($model->save()){
                Yii::$app->session->setFlash('success','Update Profile Success.');
            }
        }
        return $this->render('profile',['model'=>$model]);
    }
    public function actionUploadwall(){
        if(!empty(Yii::$app->user->getId())){
            $model=User::findOne(Yii::$app->user->getId());
            $old_wall=$model->wall_picture;
            if($model->load(Yii::$app->request->post())){
                $wall=UploadedFile::getInstance($model,'wall_picture');
                if(!empty($wall)){
                    if (!is_dir($model->getUserPath())) {
                        FileHelper::createDirectory($model->getUserPath());
                    }
                    if(!empty($old_wall)){
                        if(file_exists($model->getUserPath().$old_wall)){
                            unlink($model->getUserPath().$old_wall);
                        }
                    }
                    if($wall->saveAs(($model->getUserPath()).$wall->name)){
                        $model->wall_picture=$wall->name;
                        $model->save();
                        return $this->redirect('?r=site/profile');
                    }else{
                        echo "failed uploads"; die;
                    }
                }else{
                    return $this->redirect('?r=site/profile');
                }
            }
        }
    }
    
    public function actionUploadavatar(){
        if(!empty(Yii::$app->user->getId())){
            $model=User::findOne(Yii::$app->user->getId());
            $old_avatar=$model->profile_picture;
            if($model->load(Yii::$app->request->post())){
                $avatar=UploadedFile::getInstance($model,'profile_picture');
                if(!empty($avatar)){
                    if (!is_dir($model->getUserPath())) {
                        FileHelper::createDirectory($model->getUserPath());
                    }
                    if(!empty($old_avatar)){
                        if(file_exists($model->getUserPath().$old_avatar)){
                            unlink($model->getUserPath().$old_avatar);
                        }
                    }
                    if($avatar->saveAs(($model->getUserPath()).$avatar->name)){
                        $model->profile_picture=$avatar->name;
                        $model->save();
                        return $this->redirect('?r=site/profile');
                    }else{
                        echo "failed uploads"; die;
                    }
                }else{
                    return $this->redirect('?r=site/profile');
                }
            }
        }
    }
    

    /**
     * @inheritdoc
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }
    /**
     * Render home page of the site.
     *
     * @throws \yii\web\NotFoundHttpException
     * @return string
     */
    public function actionIndex()
    {
        /* @var $post \common\models\Post */
        $query = Post::find()
            ->from(['t' => Post::tableName()])
            ->andWhere(['status' => 'publish'])
            ->andWhere(['<=', 'date', date('Y-m-d H:i:s')])
            ->orderBy(['t.date' => SORT_DESC]);

        if (Option::get('show_on_front') == 'page' && $frontPage = Option::get('front_page')) {
            $render = '/post/view';
            $comment = new PostComment();
            $query = $query->andWhere(['id' => $frontPage]);
            if ($post = $query->one()) {
                if (is_file($this->view->theme->basePath . '/post/view-' . $post->postType->slug . '.php')) {
                    $render = '/post/view-' . $post->postType->slug;
                }

                return $this->render($render, [
                    'post' => $post,
                    'comment' => $comment,
                ]);
            }
            throw new NotFoundHttpException(Yii::t('writesdown', 'The requested page does not exist.'));
        } else {
            if (Option::get('front_post_type') !== 'all') {
                $query->innerJoinWith(['postType'])->andWhere(['name' => Option::get('front_post_type')]);
            }
            $countQuery = clone $query;
            $pages = new Pagination([
                'totalCount' => $countQuery->count(),
                'pageSize' => Option::get('posts_per_page'),
            ]);
            $query->offset($pages->offset)->limit($pages->limit);
            if ($posts = $query->all()) {
                return $this->render('index', [
                    'posts' => $posts,
                    'pages' => isset($pages) ? $pages : null,
                ]);
            }

            throw new NotFoundHttpException(Yii::t('writesdown', 'The requested page does not exist.'));
        }
    }

    /**
     * @return \yii\web\Response
     */
    public function actionLogout()
    {
        Yii::$app->user->logout();

        return $this->goHome();
    }

    /**
     * @return string|\yii\web\Response
     */
    // public function actionContact()
    // {
    //     $model = new ContactForm();

    //     if ($model->load(Yii::$app->request->post()) && $model->validate()) {
    //         if ($model->sendEmail(Option::get('admin_email'))) {
    //             Yii::$app->session->setFlash(
    //                 'success',
    //                 'Thank you for contacting us. We will respond to you as soon as possible.'
    //             );
    //         } else {
    //             Yii::$app->session->setFlash('error', 'There was an error sending email.');
    //         }

    //         return $this->refresh();
    //     }

    //     return $this->render('contact', [
    //         'model' => $model,
    //     ]);
    // }
    public function actionContactus(){
        $model= new Contact();
         if ($model->load(Yii::$app->request->post()) && $model->save()) {
            if ($model->sendEmail(Option::get('admin_email'))) {
                Yii::$app->session->setFlash(
                    'success',
                    'Thank you for contacting us. We will respond to you as soon as possible.'
                );
            } else {
                Yii::$app->session->setFlash('error', 'There was an error sending email.');
            }
            return $this->refresh();
         }
        return $this->render('contact_us',['model'=>$model]);
    }

    /**
     * Search post by title and content
     *
     * @param string $s Keyword to search posts.
     * @return string
     * @throws \yii\web\NotFoundHttpException
     */
    public function actionSearch($s)
    {
        $query = Post::find()
            ->orWhere(['like', 'title', $s])
            ->orWhere(['like', 'content', $s])
            ->andWhere(['status' => 'publish'])
            ->andWhere(['<=', 'date', date('Y-m-d H:i:s')])
            ->orderBy(['date' => SORT_DESC]);
        $countQuery = clone $query;
        $pages = new Pagination([
            'totalCount' => $countQuery->count(),
            'pageSize' => Option::get('posts_per_page'),
        ]);
        $query->offset($pages->offset)->limit($pages->limit);
        $posts = $query->all();

        if ($posts) {
            return $this->render('/site/search', [
                'posts' => $posts,
                'pages' => $pages,
                's' => $s,
            ]);
        }

        throw new NotFoundHttpException(Yii::t('writesdown', 'The requested page does not exist.'));
    }

    /**
     * @throws \yii\web\ForbiddenHttpException
     */
    public function actionForbidden()
    {
        throw new ForbiddenHttpException(Yii::t('writesdown', 'You are not allowed to perform this action.'));
    }

    /**
     * @throws \yii\web\NotFoundHttpException
     */
    public function actionNotFound()
    {
        throw new NotFoundHttpException(Yii::t('writesdown', 'The requested page does not exist.'));
    }
}
