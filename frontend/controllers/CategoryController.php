<?php

namespace frontend\controllers;
use common\models\Option;
use common\models\Term;
use Yii;
use yii\data\Pagination;
use yii\web\Controller;
use yii\web\NotFoundHttpException;

class CategoryController extends Controller{
	public function actionIndex($slug = null)
    {
        $render = 'view';
		if ($slug) {
            $model = $this->findModelBySlug($slug);
        } else {
            throw new NotFoundHttpException(Yii::t('writesdown', 'The requested page does not exist.'));
        }

        $query = $model->getPosts()
            ->andWhere(['status' => 'publish'])
            ->andWhere(['<=', 'date', date('Y-m-d H:i:s')])
            ->orderBy(['date' => SORT_DESC]);
        $countQuery = clone $query;
        $pages = new Pagination([
            'totalCount' => $countQuery->count(),
            'pageSize' => Option::get('posts_per_page'),
        ]);
        $posts = $query->offset($pages->offset)
            ->limit($pages->limit)
            ->all();

        if (is_file($this->view->theme->basePath . '/category/view-' . $model->taxonomy->name . '.php')) {
            $render = 'view-' . $model->taxonomy->name;
        }

        return $this->render($render, [
            'posts' => $posts,
            'pages' => $pages,
            'term' => $model,
        ]);
    }
    protected function findModelBySlug($slug)
    {
        $model = Term::findOne(['slug' => $slug]);

        if ($model) {
            return $model;
        }

        throw new NotFoundHttpException(Yii::t('writesdown', 'The requested page does not exist.'));
    }
}
