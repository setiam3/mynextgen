<?php
return [
    'vendorPath' => dirname(dirname(__DIR__)) . '/vendor',
    'components' => [
        'plugins' => [
            'class' => lo\plugins\components\PluginsManager::class,
            'appId' => 1, // lo\plugins\BasePlugin::APP_FRONTEND,
            // by default
            'enablePlugins' => true,
            'shortcodesParse' => true,
            'shortcodesIgnoreBlocks' => [
                '<pre[^>]*>' => '<\/pre>',
                //'<div class="content[^>]*>' => '<\/div>',
            ]
        ],
        'view' => [
            'class' => lo\plugins\components\View::class,
        ],
        'cache' => [
            'class' => 'yii\caching\FileCache',
        ],
    ],
    // 'controllerMap' => [
    //     'migrate' => [
    //         'class' => 'yii\console\controllers\MigrateController',
    //         'migrationNamespaces' => [
    //             'lo\plugins\migrations'
    //         ],
    //     ],
    // ],
];
