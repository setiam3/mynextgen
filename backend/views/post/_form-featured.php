<?php
/**
 * @link http://www.writesdown.com/
 * @author Agiel K. Saputra <13nightevil@gmail.com>
 * @copyright Copyright (c) 2015 WritesDown
 * @license http://www.writesdown.com/license/
 */
use backend\widgets\MediaModal;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
?>

   <div class="box box-primary">
        <div class="box-header with-border">
            <h3 class="box-title"><?=$model->getAttributeLabel('featured_picture') ?></h3>
            <div class="box-tools pull-right">
                <a href="#" data-widget="collapse" class="btn btn-box-tool"><i class="fa fa-minus"></i></a>
            </div>
        </div>
        <div class="box-body">
            <div class="input-group">
                <?= MediaModal::widget([
                'post' => $model->isNewRecord ? null : $model->id,
                'editor' => false,
                'buttonContent'=>'<i class="fa fa-folder-open"></i> ' . Yii::t('writesdown', 'Featured Images'),
                'multiple' => false,
                'callback'=>['name'=>'featured','value'=>'function(){}'],
                'buttonOptions' => [
                    'class' => ['btn btn-sm btn-default btn-flat'],
                ],
            ]) ?>
                <?= Html::textInput('Post[featured_picture]', '', [
                    'class' => 'form-control',
                    'placeholder' => Yii::t(
                        'writesdown', 'Featured'
                    ),
                ]) ?>
            </div>
        </div>
    </div>