<?php

$vendorDir = dirname(__DIR__);

return array (
  'codezeen/yii2-fastclick' => 
  array (
    'name' => 'codezeen/yii2-fastclick',
    'version' => '1.0.0.0',
    'alias' => 
    array (
      '@codezeen/yii2/fastclick' => $vendorDir . '/codezeen/yii2-fastclick',
    ),
  ),
  'codezeen/yii2-adminlte' => 
  array (
    'name' => 'codezeen/yii2-adminlte',
    'version' => '9999999-dev',
    'alias' => 
    array (
      '@codezeen/yii2/adminlte' => $vendorDir . '/codezeen/yii2-adminlte',
    ),
  ),
  'codezeen/yii2-tinymce' => 
  array (
    'name' => 'codezeen/yii2-tinymce',
    'version' => '9999999-dev',
    'alias' => 
    array (
      '@codezeen/yii2/tinymce' => $vendorDir . '/codezeen/yii2-tinymce',
    ),
  ),
  'yiisoft/yii2-imagine' => 
  array (
    'name' => 'yiisoft/yii2-imagine',
    'version' => '9999999-dev',
    'alias' => 
    array (
      '@yii/imagine' => $vendorDir . '/yiisoft/yii2-imagine/src',
    ),
  ),
  'yiisoft/yii2-jui' => 
  array (
    'name' => 'yiisoft/yii2-jui',
    'version' => '9999999-dev',
    'alias' => 
    array (
      '@yii/jui' => $vendorDir . '/yiisoft/yii2-jui/src',
    ),
  ),
  '2amigos/yii2-selectize-widget' => 
  array (
    'name' => '2amigos/yii2-selectize-widget',
    'version' => '1.1.0.0',
    'alias' => 
    array (
      '@dosamigos/selectize' => $vendorDir . '/2amigos/yii2-selectize-widget/src',
    ),
  ),
  '2amigos/yii2-date-time-picker-widget' => 
  array (
    'name' => '2amigos/yii2-date-time-picker-widget',
    'version' => '9999999-dev',
    'alias' => 
    array (
      '@dosamigos/datetimepicker' => $vendorDir . '/2amigos/yii2-date-time-picker-widget/src',
    ),
  ),
  '2amigos/yii2-gallery-widget' => 
  array (
    'name' => '2amigos/yii2-gallery-widget',
    'version' => '1.1.0.0',
    'alias' => 
    array (
      '@dosamigos/gallery' => $vendorDir . '/2amigos/yii2-gallery-widget/src',
    ),
  ),
  'rmrevin/yii2-fontawesome' => 
  array (
    'name' => 'rmrevin/yii2-fontawesome',
    'version' => '2.9999999.9999999.9999999-dev',
    'alias' => 
    array (
      '@rmrevin/yii/fontawesome' => $vendorDir . '/rmrevin/yii2-fontawesome',
    ),
  ),
  'loveorigami/yii2-jsoneditor' => 
  array (
    'name' => 'loveorigami/yii2-jsoneditor',
    'version' => '9999999-dev',
    'alias' => 
    array (
      '@lo/widgets' => $vendorDir . '/loveorigami/yii2-jsoneditor',
    ),
  ),
  'loveorigami/yii2-plugins-system' => 
  array (
    'name' => 'loveorigami/yii2-plugins-system',
    'version' => '9999999-dev',
    'alias' => 
    array (
      '@lo/plugins' => $vendorDir . '/loveorigami/yii2-plugins-system/src',
    ),
  ),
  'loveorigami/yii2-shortcodes-pack' => 
  array (
    'name' => 'loveorigami/yii2-shortcodes-pack',
    'version' => '9999999-dev',
    'alias' => 
    array (
      '@lo/shortcodes' => $vendorDir . '/loveorigami/yii2-shortcodes-pack/src',
    ),
  ),
  '2amigos/yii2-file-upload-widget' => 
  array (
    'name' => '2amigos/yii2-file-upload-widget',
    'version' => '9999999-dev',
    'alias' => 
    array (
      '@dosamigos/fileupload' => $vendorDir . '/2amigos/yii2-file-upload-widget/src',
    ),
  ),
  'cebe/yii2-gravatar' => 
  array (
    'name' => 'cebe/yii2-gravatar',
    'version' => '9999999-dev',
    'alias' => 
    array (
      '@cebe/gravatar' => $vendorDir . '/cebe/yii2-gravatar/cebe/gravatar',
    ),
  ),
  'yiisoft/yii2-bootstrap' => 
  array (
    'name' => 'yiisoft/yii2-bootstrap',
    'version' => '9999999-dev',
    'alias' => 
    array (
      '@yii/bootstrap' => $vendorDir . '/yiisoft/yii2-bootstrap/src',
    ),
  ),
  'yiisoft/yii2-codeception' => 
  array (
    'name' => 'yiisoft/yii2-codeception',
    'version' => '9999999-dev',
    'alias' => 
    array (
      '@yii/codeception' => $vendorDir . '/yiisoft/yii2-codeception',
    ),
  ),
  'yiisoft/yii2-debug' => 
  array (
    'name' => 'yiisoft/yii2-debug',
    'version' => '9999999-dev',
    'alias' => 
    array (
      '@yii/debug' => $vendorDir . '/yiisoft/yii2-debug/src',
    ),
  ),
  'yiisoft/yii2-faker' => 
  array (
    'name' => 'yiisoft/yii2-faker',
    'version' => '9999999-dev',
    'alias' => 
    array (
      '@yii/faker' => $vendorDir . '/yiisoft/yii2-faker/src',
    ),
  ),
  'yiisoft/yii2-gii' => 
  array (
    'name' => 'yiisoft/yii2-gii',
    'version' => '9999999-dev',
    'alias' => 
    array (
      '@yii/gii' => $vendorDir . '/yiisoft/yii2-gii/src',
    ),
  ),
  'yiisoft/yii2-swiftmailer' => 
  array (
    'name' => 'yiisoft/yii2-swiftmailer',
    'version' => '9999999-dev',
    'alias' => 
    array (
      '@yii/swiftmailer' => $vendorDir . '/yiisoft/yii2-swiftmailer/src',
    ),
  ),
);
