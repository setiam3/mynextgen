<?php 
use codezeen\yii2\adminlte\widgets\Alert;
use common\models\Option;
use yii\bootstrap\ActiveForm;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\web\View;
$this->title="LOGIN";
$js="
$('#wrapper').removeClass();
$('#wrapper').children().removeClass();
$('#content').children().removeClass();
";
$this->registerJS($js,View::POS_READY,'remove');
?>
<div class="row">
<div class="col-md-9 hidden-xs">
	<div id="carousel-login" class="carousel slide" data-ride="carousel">
  <div class="carousel-inner" role="listbox">
    <div class="item active">
		<img src="uploads/2019/10/the-color-run-4477874-1920.jpg">
<div style="position: absolute;top: 0;background: greenyellow;opacity: 0.2;width: 100%;height: 100%;"></div>
      <div class="carousel-caption" style="top:15rem">
        <h1 class="text-left"><strong></strong><?=strtoupper(Option::get('sitetitle'))?></strong></h1>
        <p class="text-left">Tackling loneliness and isolation, especially in older people, is a National Priority. Barnet is projected to have some of the strongest growth in elderly residents out of all London Boroughs.</p>
      </div>
    </div>
  </div>
</div>
</div>
<div class="col-md-3">
	<h1><?=$this->title?></h1>
	<p>
	Enter Your Email address and password after that you can communicate with each other</p>
<br>
	<div class="login-box">
    <?= Alert::widget() ?>

    <div class="login-box-body">

        <?php $form = ActiveForm::begin(['id' => 'login-form']) ?>

        <?= $form->field($model, 'username', [
            'template' => '<label class="">Username or Phone Number</label>
            <div class="input-group">
            	<div class="input-group-addon" style="background:#46c4c1;color:#fff;border:none"><i class="fa fa-envelope"></i></div>{input}</div>{error}',
        ])->textInput(['placeholder' => $model->getAttributeLabel('username'),'style'=>'border:solid 1px #46c4c1;']) ?>

        <?= $form->field($model, 'password', [
            'template' => '
	<label class="">Password</label>
            <div class="input-group">
            	<div class="input-group-addon" style="background:#46c4c1;color:#fff;border:none"><span class="glyphicon glyphicon-lock"></span></div>{input}</div>{error}
',
        ])->passwordInput(['placeholder' => $model->getAttributeLabel('password'),'style'=>'border:solid 1px #46c4c1;']) ?>

        <div class="row" style="font-size: 14px;">
            <div class="col-xs-6" style="padding-right: 0">
                <?= $form->field($model, 'rememberMe')->checkbox() ?>
            </div>
            <div class="col-xs-6" style="padding-right: 0;padding-top: 10px">
            |	<?= Html::a(Yii::t('writesdown', 'Forgot Password'), ['request-password-reset']) ?>
            </div>
        </div>
        <div class="">
        	 <?= Html::submitButton('Login', [
                    'class' => 'btn btn-tosca btn-block btn-flat',
                    'name' => 'signin-button',
                ]) ?>
        </div>
        <?php ActiveForm::end() ?>
        <br/>
       
    </div>
    <p class="text-center">Or</p>
<div class="row">
    <ul class="p-1 list-inline" style="">
            <li class="facebook list-item rounded-pill" style="border:solid 1px #eee">
                <a href="#"><i class="fa fa-facebook"></i> </a>
            </li>
            <li class="google-plus list-item rounded-pill" style="border:solid 1px #eee">
                <a href="#"><i class="fa fa-google"></i> </a>
            </li>
            <li class="twitter list-item rounded-pill" style="border:solid 1px #eee">
                <a href="#"><i class="fa fa-twitter"></i> </a>
            </li>
            
        </ul></div>
    <div class="row p-1">
    	Don't have an account? 
		<a href="<?=Url::to('?r=site/register')?>">Create</a>
    </div>

</div>
</div>
</div>
<style>
	.navbar,footer{
		display: none;
	}
	#content{
		margin:0 auto;
	}
</style>