<?php 
use common\models\Option;
use yii\bootstrap\ActiveForm;
use yii\bootstrap\Modal;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\web\View;
use codezeen\yii2\tinymce\TinyMce;
use yii\captcha\Captcha;
use themes\nextgen\classes\assets\ThemeAsset;

$this->title="Contact Us";
$js="
var nextbtn1=$('#modal-message .btnnext');
var nextbtn2=$('#modal-voice .btnnext');
var cancel=$('.cancel');
var submit=$('.submit');
var message=$('.messages');
var name=$('#contact-name');
var email=$('#contact-email');
var voice=$('.voice');
message.hide();submit.hide();voice.hide();
cancel.hide();
function isInvalid(node) {
    return node.attr('aria-invalid');
  }
    nextbtn1.click(function () {
        if(isInvalid(name)==true && isInvalid(email)==true){
        }else if(name.val()!=='' && email.val()!==''){
            message.show();
            submit.show();
            cancel.show();
            nextbtn1.hide();
        }
    });
    nextbtn2.click(function () {
        if(isInvalid($('#modal-voice #contact-name'))==true && isInvalid($('#modal-voice #contact-email'))==true){
        }else if($('#modal-voice #contact-name').val()!=='' && $('#modal-voice #contact-email').val()!==''){
            voice.show();
            submit.show();
            cancel.show();
            nextbtn2.hide();
        }
    });
    $('#modal-message').on('hide.bs.modal', function () {
        submit.hide();
        cancel.hide();
    });
    $('#modal-message').on('show.bs.modal', function () {
        message.hide();submit.hide();cancel.hide();
        $('#modal-message .btnnext').show();
    });
    $('#modal-voice').on('hide.bs.modal', function () {
        submit.hide();
        cancel.hide();
    });
    $('#modal-voice').on('show.bs.modal', function () {
        voice.hide();
        $('#modal-voice .btnnext').show();
    });
    cancel.click(function(){
        tinymce.activeEditor.setContent('');
    });
";
$this->registerJs($js);
//$this->registerJsFile('/public/assets/19028d00/js/wavesurfer.min.js');
//$this->registerJsFile('https://raw.githubusercontent.com/addpipe/simple-recorderjs-demo/master/js/app.js');
//$this->registerJsFile('/public/assets/19028d00/js/podcast.js');
//$this->registerJsFile('/public/assets/19028d00/js/jquery-latest.min.js',['position'=>View::POS_HEAD]);
//$this->registerAssetBundle(yii\web\JqueryAsset::className(), View::POS_HEAD);
$assetBundle = ThemeAsset::register($this);
$assetBundle->js=[
    'js/recorder.js',
    //'js/wavesurfer.min.js',
    'js/podcast.js',
    //'js/jquery-latest.min.js'
];
//$assetBundle->jsOptions=['position'=>View::POS_HEAD];
//$assetBundle->depends = [
//    'yii\web\YiiAsset',
    //'yii\bootstrap\BootstrapAsset',
    //'yii\bootstrap\BootstrapPluginAsset',
    //'rmrevin\yii\fontawesome\AssetBundle',
//];
?>
<div class="row">
    <div class="col-md-7">
        <h1>STAY IN TOUCH IF YOU FIND ANY PROBLEMS OR OTHER THINGS TELL US</h1>
        <BR>
        <div class="align-center">
        <?php
Modal::begin([
         'options'=>['style'=>'opacity:0.98','id'=>'modal-message'],
         'size'=>Modal::SIZE_LARGE,
         'closeButton'=>false,
         'toggleButton' => ['label' => '<i class="glyphicon glyphicon-text-width"></i> Send Text Message','class'=>'btn rounded-pill btn-tosca'],
     ]);
     $form = ActiveForm::begin(['id' => 'messages-form','options'=>['style'=>'height:40rem']]);
     echo '<div class="col-12">';
     echo $form->field($model, 'name', [
         'template' => '<label class="">Name</label>
         <div class="input-group">
             <div class="input-group-addon" style="background:#46c4c1;color:#fff;border:none"><i class="fa fa-user"></i></div>{input}</div>{error}',
             'options'=>['class'=>'col-md-6'],
     ])->textInput(['placeholder' => $model->getAttributeLabel('name'),'style'=>'border:solid 1px #46c4c1;']);

     echo $form->field($model, 'email', [
         'template' => '
 <label class="">Email</label>
         <div class="input-group">
             <div class="input-group-addon" style="background:#46c4c1;color:#fff;border:none"><i class="fa fa-envelope"></i></div>{input}</div>{error}
','options'=>['class'=>'col-md-6'],
     ])->textInput(['placeholder' => $model->getAttributeLabel('email'),'style'=>'border:solid 1px #46c4c1;']);
echo '</div>';
echo '<div class="col-md-12 messages">';
echo $form->field($model, 'messages')->widget(
    TinyMce::className(),
    [
        'settings'        => [
            'language'               => 'en',
            'plugins'                => [
                "advlist autolink lists link image charmap print preview hr anchor pagebreak",
                "searchreplace visualblocks visualchars code fullscreen",
                "insertdatetime media nonbreaking save table contextmenu directionality",
                "template paste textcolor"
            ],
            'menubar'=>false,
            'toolbar'                =>"insertfile undo redo | styleselect |cut copy paste | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image | media ",
            'toolbar_items_size'     => 'small',
            'image_advtab'           => true,
            'relative_urls'          => false,
        ],
    ]
    );
echo '</div>';
//echo $form->field($model, 'verifyCode')->widget(Captcha::className(), ['template' => '<div class="row"><div class="col-lg-3">{image}</div><div class="col-lg-6">{input}</div></div>',]);
    echo '<div class="col-md-12">';
    echo Html::button('Next', [
                 'class' => 'btn btn-pink btn-default btn-flat btnnext',
                 'name' => 'next-button',
             ]);
    echo Html::button('Cancel', [
                 'class' => 'btn btn-default btn-flat cancel',
                 'name' => 'cancel-button',
             ]);
    echo Html::submitButton('Send', ['class' => 'btn submit btn-pink', 'name' => 'contact-button']);
    echo '</div>';
    ActiveForm::end();
     Modal::end();
     echo ' Or ';
Modal::begin([
         'options'=>['style'=>'opacity:0.98','id'=>'modal-voice'],
         'size'=>Modal::SIZE_LARGE, 'closeButton'=>false,
         'toggleButton' => ['label' => '<i class="fa fa-microphone"></i> Send a Voice Message','class'=>'btn btn-pink rounded-pill'],
     ]);
     $form = ActiveForm::begin(['id' => 'voice-form','options'=>['style'=>'height:40rem']]);
     echo '<div class="col-12">';
     echo $form->field($model, 'name', [
         'template' => '<label class="">Name</label>
         <div class="input-group">
             <div class="input-group-addon" style="background:#46c4c1;color:#fff;border:none"><i class="fa fa-user"></i></div>{input}</div>{error}',
             'options'=>['class'=>'col-md-6'],
     ])->textInput(['placeholder' => $model->getAttributeLabel('name'),'style'=>'border:solid 1px #46c4c1;']);

     echo $form->field($model, 'email', [
         'template' => '
 <label class="">Email</label>
         <div class="input-group">
             <div class="input-group-addon" style="background:#46c4c1;color:#fff;border:none"><i class="fa fa-envelope"></i></div>{input}</div>{error}
','options'=>['class'=>'col-md-6'],
     ])->textInput(['placeholder' => $model->getAttributeLabel('email'),'style'=>'border:solid 1px #46c4c1;']);
echo '</div>';
?><div class="col-md-12 voice">
<label class="control-label" for="contact-voice_message">Voice Message</label>
      <input type="hidden" name="audio64" id="audio64" accept="audio/*;capture=microphone">
      <div id="controls" class="m-4">
     <button type="button" id="recordButton" class="btn gradation-bg"><i class="fa fa-microphone-alt"></i></button>
     <button type="button" id="pauseButton" class="btn gradation-bg" disabled><i class="fa fa-pause-circle"></i></button>
     <button type="button" id="stopButton" class="btn gradation-bg" disabled><i class="fa fa-stop-circle"></i></button>
    </div>
  <div id="waveform" style="display: none;margin-bottom: 1rem;"></div>
  <div id="formats" style="display: none;">Format: start recording to see sample rate</div>
  <div class="controls mb-3" style="display: none;">
        <button type="button" class="btn gradation-bg" data-action="back"><i class="fa fa-backward"></i></button>
        <button type="button" class="btn gradation-bg" data-action="play"><i class="fa fa-play-circle"></i></button>
        <button type="button" class="btn gradation-bg" data-action="forward"><i class="fa fa-forward"></i></button>
        <button type="button" class="btn gradation-bg" data-action="mute"><i class="fa fa-volume-up"></i></button>
        <button type="reset" class="btn gradation-bg" data-action="reset"><i class="fa fa-times"></i></button>
      </div>

</div>
<?php
    echo '<div class="col-md-12">';
    echo Html::button('Next', [
                 'class' => 'btn btn-pink btn-default btn-flat btnnext',
                 'name' => 'next-button',
             ]);
    echo Html::button('Cancel', [
                 'class' => 'btn btn-default btn-flat cancel',
                 'name' => 'cancel-button',
             ]);
    echo Html::submitButton('Send', ['class' => 'btn submit btn-pink', 'name' => 'contact-button']);
    echo '</div>';
    ActiveForm::end();
     Modal::end();
?>
        </div>
    </div>
    <div class="col-md-5">
        <img class="align-center" src="https://dummyimage.com/400x400/eeeeee/ffffff.png"/>
    </div>
</div>
<!-- <script src="/public/assets/19028d00/js/recorder.js"></script> -->
<style>
	footer{
		display: none;
	}
</style>