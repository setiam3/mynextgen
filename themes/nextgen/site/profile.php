<?php 
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\bootstrap\Modal;
?>
<div class="row">
<div class="col-md-8">
    
    <div class="wall-picture" style="position:absolute">
        <?php echo (empty($model->wall_picture))?'<img class="align-center" src="https://dummyimage.com/800x400/eeeeee/ffffff.png"/>':
        Html::img($model->getUserUrl().$model->wall_picture);?>
        <div style="position: absolute;top: 0;background: aqua;opacity: 0.2;width: 100%;height: 100%;"></div>
    </div>
    <div class="bt-wallpicture" style="">
                <?php Modal::begin([
                    'options'=>['style'=>'opacity:0.9','id'=>'modal-wallpaper'],
                    'size'=>Modal::SIZE_LARGE,
                    'closeButton'=>false,
                    'toggleButton' => ['tag'=>'a','label' => '<span class="plus p-1" style="position: absolute;top: -10px;right: 12px;">+</span><i class="fa fa-camera p-1"></i>','class'=>'','style'=>'position:absolute;right:0;top:0;z-index:1;color:#fff'],
                ]);
                $form = ActiveForm::begin(['action'=>Yii::$app->request->baseUrl.'/?r=site/uploadwall','id' => 'messages-form','options'=>['style'=>'height:40rem']]);
                    echo $form->field($model,'wall_picture')->fileInput();
                    echo Html::submitButton('Save');
                ActiveForm::end();
                Modal::end();?>
            </div>
    <div class="row profile-content">
        <div class="col-sm-4">
            <div class="profile-avatar">
            <?php 
            echo (empty($model->profile_picture))?'<img class="align-center rounded-circle border-5" style="" src="https://dummyimage.com/300x300/000/ffffff.png"/>':
            Html::img($model->getUserUrl().$model->profile_picture,['class'=>'center rounded-circle border-5']);
            ?>
            <?php 
                Modal::begin([
                    'options'=>['style'=>'opacity:0.9','id'=>'modal-photo'],
                    'size'=>Modal::SIZE_LARGE,
                    'closeButton'=>false,
                    'toggleButton' => ['tag'=>'a','label' => '<span style="position: absolute;top: -10px;right: 30px;" class="plus">+</span><i class="fa fa-camera"></i>','style'=>"position: absolute;right: 0;bottom: 0;border: solid 5px #fff;color:#554646;border-radius: 50%;padding: 5px 10px;background: #fff;font-size: 3rem;"],
                ]);
                $form = ActiveForm::begin(['action'=>Yii::$app->request->baseUrl.'/?r=site/uploadavatar','id' => 'messages-form','options'=>['style'=>'height:40rem']]);
                echo $form->field($model,'profile_picture')->fileInput();
                echo Html::submitButton('Save');
                ActiveForm::end();
                Modal::end();
            ?>
            </div>
        </div>
        <div class="col-sm-8">
            <div class="col-md-12 profile-head">
                <h1 style="color:#fff;"><?=$model->username?></h1>
                <p style="color:#fff"><?=$model->email?></p>
                <br>
            </div>
            <div class="col-md-12">
                <ul class="list-inline profile-btn">
                    <li class="list-item">
                    <?php Modal::begin([
                    'options'=>['style'=>'opacity:0.9','id'=>'modal-setting'],
                    'size'=>Modal::SIZE_LARGE,
                    'closeButton'=>false,
                    'toggleButton' => ['label' => '<span class="glyphicon glyphicon-cog"></span> Setting','class'=>"btn btn-blue rounded-pill"],
                ]);
                $form = ActiveForm::begin(['id' => 'messages-form','options'=>['style'=>'height:40rem']]);
                    echo $form->field($model,'full_name',[
                        'template' => '<label class="">'.$model->getAttributeLabel('full_name').'</label>
            <div class="input-group">
                <div class="input-group-addon" style="background:#46c4c1;color:#fff;border:none"><i class="fa fa-user"></i></div>{input}</div>{error}',
                'options'=>['class'=>'col-md-6'],
                    ])->textInput(['placeholder' => $model->getAttributeLabel('full_name'),'style'=>'border:solid 1px #46c4c1;']);
                    echo $form->field($model,'email',[
                        'template' => '<label class="">'.$model->getAttributeLabel('email').'</label>
            <div class="input-group">
                <div class="input-group-addon" style="background:#46c4c1;color:#fff;border:none"><i class="fa fa-envelope"></i></div>{input}</div>{error}',
                'options'=>['class'=>'col-md-6'],
                    ]);
                    echo $form->field($model,'phone',[
                        'template' => '<label class="">'.$model->getAttributeLabel('phone').'</label>
            <div class="input-group">
                <div class="input-group-addon" style="background:#46c4c1;color:#fff;border:none"><i class="fa fa-phone"></i></div>{input}</div>{error}',
                'options'=>['class'=>'col-md-6'],
                    ]);
                    echo $form->field($model,'password',[
                        'template' => '<label class="">'.$model->getAttributeLabel('password').'</label>
            <div class="input-group">
                <div class="input-group-addon" style="background:#46c4c1;color:#fff;border:none"><span class="glyphicon glyphicon-lock"></span></div>{input}</div>{error}',
                'options'=>['class'=>'col-md-6'],
                    ])->passwordInput();
                    echo '<button type="button" class="btn btn-default m-1" data-dismiss="modal">Cancel</button>';
            echo Html::submitButton('Save <i class="fa fa-arrow-right"></i>', ['class' => 'btn submit btn-tosca m-1', 'name' => 'profile-button']);
                ActiveForm::end();
                Modal::end();
                ?>
                    </li>
                    <li class="list-item">
                    <?=Html::beginForm(['/site/logout'], 'post',[])
                    . Html::submitButton(
                        'Logout',
                        ['class' => 'btn logout btn-pink rounded-pill']
                    )
                    . Html::endForm()?>
                    </li>
                </ul>
            </div>
        </div>
        </div>
</div>
<div class="col-md-4 widget-chat">
    <h4 class="tosca m-0">Joined to your conversation</h4>
    <ul class="list-down" >
        <li>asd</li>
        <li>asd</li>
        <li>asd</li>
        <li>asd</li>
        <li>asd</li>
        <li>asd</li>
        <li>asd</li>
        <li>asd</li>
        <li>asd</li>
        <li>asd</li>
        <li>asd</li>
    </ul>
</div>
</div>
<div class="row post-slider">
    <p class="p-1"><b>Your Conversation</b></p>
    <div class="col-sm-2 p-1">
        <div class="card">card</div>
        <!-- <img class="align-center" src="https://dummyimage.com/300x300/000/ffffff.png"/> -->
    </div>
    <div class="col-sm-2 p-1">
        <img class="align-center" src="https://dummyimage.com/300x300/000/ffffff.png"/>
    </div>
    <div class="col-sm-2 p-1">
        <img class="align-center" src="https://dummyimage.com/300x300/000/ffffff.png"/>
    </div>
    <div class="col-sm-2 p-1">
        <img class="align-center" src="https://dummyimage.com/300x300/000/ffffff.png"/>
    </div>
    <div class="col-sm-2 p-1">
        <img class="align-center" src="https://dummyimage.com/300x300/000/ffffff.png"/>
    </div>
    <div class="col-sm-2 p-1">
        <img class="align-center" src="https://dummyimage.com/300x300/000/ffffff.png"/>
    </div>
</div>