<?php
/**
 * @link http://www.nextgen.com/
 * @author Agiel K. Saputra <13nightevil@gmail.com>
 * @copyright Copyright (c) 2015 nextgen
 * @license http://www.nextgen.com/license/
 */

return [
    'backend' => [
        'menu' => [
            'location' => [
                'primary' => 'Primary',
            ],
        ],
        'postType' => [
            'post' => [
                'meta' => [
                    ['class' => 'themes\nextgen\classes\meta\Meta'],
                ],
                'support' => [],
            ],
            'page' => [
                'meta' => [
                    ['class' => 'themes\nextgen\classes\meta\Meta'],
                ],
                'support' => [],
            ],
        ],
        'widget' => [
            [
                'title' => 'Sidebar',
                'description' => 'Main sidebar that appears on the right.',
                'location' => 'sidebar',
            ],
            [
                'title' => 'Footer Left',
                'description' => 'Appears on the left of footer',
                'location' => 'footer-left',
            ],
            [
                'title' => 'Footer Middle',
                'description' => 'Appears on the middle of footer',
                'location' => 'footer-middle',
            ],
            [
                'title' => 'Footer Right',
                'description' => 'Appears on the right of footer',
                'location' => 'footer-right',
            ],
        ],
    ],
];
