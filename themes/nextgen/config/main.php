<?php
/**
 * @link http://www.writesdown.com/
 * @author Agiel K. Saputra <13nightevil@gmail.com>
 * @copyright Copyright (c) 2015 WritesDown
 * @license http://www.writesdown.com/license/
 */

return [
    'info' => [
        'Name' => 'Next Generation Theme',
        'URI' => 'http://www.nextgen.com',
        'Author' => 'Wawa',
        'Author URI' => 'http://www.zetia.web.id',
        'Description' => 'Default theme for Nextgen',
        'Version' => '1.0',
        'License' => 'http://www.nextgen.com/license',
        'License URI' => 'http://www.nextgen.com/license',
        'Tags' => ' bootstrap',
        'Text Domain' => 'nextgentheme',
    ],
];
