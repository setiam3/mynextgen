<?php
/**
 * @link http://www.writesdown.com/
 * @author Agiel K. Saputra <13nightevil@gmail.com>
 * @copyright Copyright (c) 2015 WritesDown
 * @license http://www.writesdown.com/license/
 */
use yii\helpers\Html;
use frontend\widgets\RenderWidget;
//use themes\nextgen\classes\widgets\Nav;
use common\models\Menu;

/* @var $this yii\web\View */
/* @var $posts common\models\Post[] */
?>
<footer id="footer-primary">
    <div class="container ">
        <div class="row">
            <div class="col-sm-3">
                <?= RenderWidget::widget([
                    'location' => 'footer-left',
                    'config' => [
                        'beforeWidget' => '<div class="widget">',
                        'afterWidget' => '</div>',
                        'beforeTitle' => '<div class="widget-title"> <h4>',
                        'afterTitle' => '</div></h4>',
                    ],
                ]) ?>

            </div>
            <div class="col-sm-3">
                <?= RenderWidget::widget([
                    'location' => 'footer-middle',
                    'config' => [
                        'beforeWidget' => '<div class="widget">',
                        'afterWidget' => '</div>',
                        'beforeTitle' => '<div class="widget-title"> <h4>',
                        'afterTitle' => '</div></h4>',
                    ],
                ]) ?>

            </div>
            <div class="col-sm-6">
                <?= RenderWidget::widget([
                    'location' => 'footer-right',
                    'config' => [
                        'beforeWidget' => '<div class="widget">',
                        'afterWidget' => '</div>',
                        'beforeTitle' => '<div class="widget-title"> <h4>',
                        'afterTitle' => '</div></h4>',
                    ],
                ]) ?>

            </div>
        </div>
    </div>
    <div class="container mt-2 ">
            <div class="col-sm-6">
        <img src="<?=$assetBundle->baseUrl.'/img/logo-black-60.png'?>" alt="Logo-black" style="width: 5rem;">
            Copyright &copy; 2019 <?=strtoupper($siteTitle)?></div>
            <div class="col-sm-6">
                <?php 
                
                foreach (Menu::get('bottom') as $values){

                    $value=(object)$values;
                    $ar[]=['url'=>$value->url,
                        'label'=>$value->label,
                    ];
                }
                
                echo yii\bootstrap\Nav::widget([
                'activateParents' => true,
                'options' => ['class' => 'list-inline','style'=>'margin: 2rem 0;'],
                'items' => Menu::get('bottom'),
                'encodeLabels' => false,
            ]);
             ?>
        <!-- <ul class="pull-left list-inline" style="margin: 2rem 0;">
            <li class="facebook list-item rounded-pill" style="background-color: #ffeebb">
                <a href="#"><i class="fa fa-facebook"></i> </a>
            </li>
            <li class="twitter list-item rounded-pill" style="background-color: #ffeebb">
                <a href="#"><i class="fa fa-twitter"></i> </a>
            </li>
            <li class="google-plus list-item rounded-pill" style="background-color: #ffeebb">
                <a href="#"><i class="fa fa-google-plus"></i> </a>
            </li>
            <li class="youtube list-item rounded-pill" style="background-color: #ffeebb">
                <a href="#"><i class="fa fa-youtube"></i> </a>
            </li>
            <li class="rss list-item rounded-pill" style="background-color: #ffeebb">
                <?= Html::a('<i class="fa fa-rss"></i>', ['/feed']) ?>
            </li>
        </ul> -->
    </div>
    </div>
</footer>
<?php $this->registerJs('(function($){$(".widget ul").addClass("nav")})(jQuery);', $this::POS_END) ?>
