<?php
/**
 * @link http://www.nextgen.com/
 * @author Agiel K. Saputra <13nightevil@gmail.com>
 * @copyright Copyright (c) 2015 nextgen
 * @license http://www.nextgen.com/license/
 */
use common\models\Menu;
use common\models\User;
use themes\nextgen\classes\widgets\Nav;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;
use yii\widgets\Breadcrumbs;
/* @var $this yii\web\View */
/* @var $assetBundle themes\nextgen\classes\assets\ThemeAsset */
/* @var $siteTitle string */
/* @var $tagLine string */
$model=User::findOne(Yii::$app->user->getId());
$logins=
    Yii::$app->user->isGuest ? array(
        ['label' => 'Sign In', 'url' => ['/site/login']],
        ['label' => 'Sign Up', 'url' => ['/site/register'],'linkOptions'=>['class'=>'btn-pink rounded-pill signup','style'=>'padding: 10px 20px 10px 20px;margin-top: 2rem;']]
        ):array(
            ['label'=>'<i class="fa fa-bell-o"></i><span class="label label-warning">10</span>','dropDownCaret'=>'a',
        'items'=>[
            'label'=>'item1','url'=>'#'
        ]],
        ['label' => Html::img($model->getUserUrl().$model->profile_picture,['style'=>'width:3rem;border-radius: 50%;border: 1px solid;margin-right: 10px;']).
        '<span class="hidden-sm">'.Yii::$app->user->identity->username.'</span>', 'url' => ['/#'],'linkOptions'=>['class'=>'dropdown-toggle','data-toggle'=>'dropdown'],
        'items'=>[
            ['label' => 'Profile', 'url' => ['/site/profile'],'options'=>['class'=>'text-center']],
            ('<li>'
                . Html::beginForm(['/site/logout'], 'post',[
                    'style'=>'display: table;margin: 0 auto;'
])
                . Html::submitButton(
                    'Logout',
                    ['class' => 'btn btn-link logout']
                )
                . Html::endForm()
                . '</li>')
        ]
    ],
    )
;

?>

<nav id="navbar-primary" class="navbar navbar-default navbar-static-top border0">
    <div class="container">
        <div class="navbar-header">
            <button aria-expanded="false" data-target="#menu-primary" data-toggle="collapse"
                    class="navbar-toggle collapsed" type="button">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <?php $brandTag = Yii::$app->controller->route == 'site/index' ? 'h1' : 'div' ?>
            <?= Html::beginTag($brandTag, ['class' => 'navbar-brand']) ?>

            <a href="<?= Url::base(true) ?>">
                <img src="<?= $assetBundle->baseUrl . '/img/logo-100.png' ?>" alt="Website Logo">
                <!-- <span><?= Html::encode($siteTitle) ?></span> -->
            </a>
            <?= Html::endTag($brandTag) ?>

        </div>
        <div id="menu-primary" class="collapse navbar-collapse">
            <?= Nav::widget([
                'activateParents' => true,
                'options' => ['class' => 'nav navbar-nav navbar-left'],
                'items' => Menu::get('primary'),
                'encodeLabels' => false,
            ]) ?>

            <?= Nav::widget([
                'activateParents' => true,
                'options' => ['class' => 'nav navbar-nav navbar-right'],
                'items' => $logins,
                'encodeLabels' => false,
            ]) ?>
            <div class="search navbar-right" style="padding:2.4rem 0 0 0;">
                <?php $form = ActiveForm::begin([
                    'action' => Url::to(['/site/search']),
                    'method' => 'get',
                    'id' => 'search-form-top',
                    'options' => ['class' => 'form-search'],
                ]) ?>

                <div class="input-group rounded-pill">
                    <?= Html::textInput('s', Yii::$app->request->get('s'), [
                        'class' => 'form-control rounded-pill',
                        'placeholder' => 'Search Everything Here ...',
                        'style'=>'width:17vw;'
                    ]) ?>

                    <span class="icon-inside">
                        <i class="fa fa-search"></i>
                    </span>
                </div>
                <?php ActiveForm::end() ?>

            </div>
        </div>
    </div>
</nav>
