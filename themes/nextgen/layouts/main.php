<?php
/**
 * @link http://www.nextgen.com/
 * @author Agiel K. Saputra <13nightevil@gmail.com>
 * @copyright Copyright (c) 2015 nextgen
 * @license http://www.nextgen.com/license/
 */

use common\models\Option;
use themes\nextgen\classes\assets\ThemeAsset;
use yii\helpers\Html;
use frontend\widgets\Alert;
$assetBundle = ThemeAsset::register($this);

/* @var $this \yii\web\View */
/* @var $content string */

// Canonical
$this->registerLinkTag(['rel' => 'canonical', 'href' => Yii::$app->request->absoluteUrl]);

// Favicon
$this->registerLinkTag([
    'rel' => 'icon',
    'href' => $assetBundle->baseUrl . '/img/favicon.ico',
    'type' => 'image/x-icon',
]);
$js=<<<JS
if (document.location.protocol != "https:") {document.location = document.URL.replace(/^http:/i, "https:");}
$(".alert").animate({opacity: 1.0}, 3000).fadeOut("slow");
JS;
$this->registerJs($js,yii\web\View::POS_READY);
// Add meta robots noindex, nofollow when option disable_site_indexing = true
if (Option::get('disable_site_indexing')) {
    $this->registerMetaTag(['name' => 'robots', 'content' => 'noindex, nofollow']);
}

// Get site-title and tag-line
$siteTitle = Option::get('sitetitle');
$tagLine = Option::get('tagline');
$this->beginPage();
?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?= Html::csrfMetaTags() ?>
    <title>
        <?= $this->title ?>
    </title>
    <?php $this->head() ?>
</head>
<body>
<?php $this->beginBody() 
?>
<?= $this->render('header', [
    'assetBundle' => $assetBundle,
    'siteTitle' => $siteTitle,
    'tagLine' => $tagLine,
]) ?>
<div class="container" id="wrapper">
    <div class="row">
        <!-- <div class="col-md-8"> -->
            <div id="content">
            <?= Alert::widget() ?>
                <?= $content ?>
                
            </div>
        <!-- </div> -->
        <!-- <?= $this->render('sidebar') ?> -->
    </div>
</div>
<?= $this->render('footer',['assetBundle' => $assetBundle,'siteTitle' => $siteTitle,
]) ?>
<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
