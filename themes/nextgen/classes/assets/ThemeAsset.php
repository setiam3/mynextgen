<?php
/**
 * @link http://www.nextgen.com/
 * @copyright Copyright (c) 2015 nextgen
 * @license http://www.nextgen.com/license/
 */

namespace themes\nextgen\classes\assets;

use yii\web\AssetBundle;

/**
 * Register asset files for nextgen default theme.
 *
 * @author Agiel K. Saputra <13nightevil@gmail.com>
 * @since 0.1.0
 */
class ThemeAsset extends AssetBundle
{
    public $sourcePath = '@themes/nextgen/assets';
    public $depends = [
        'yii\web\YiiAsset',
        'yii\bootstrap\BootstrapAsset',
        'yii\bootstrap\BootstrapPluginAsset',
        'rmrevin\yii\fontawesome\AssetBundle',
    ];
    public function init()
    {
        if (YII_DEBUG) {
            $this->css = ['css/site.css'];
        } else {
            $this->css = ['css/site.min.css'];
        }
    }
}
