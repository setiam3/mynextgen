<?php
/**
 * @link http://www.nextgen.com/
 * @author Agiel K. Saputra <13nightevil@gmail.com>
 * @copyright Copyright (c) 2015 nextgen
 * @license http://www.nextgen.com/license/
 */

namespace themes\nextgen\classes\widgets;

use Yii;

/**
 * Class Nav
 *
 * @author Agiel K. Saputra <13nightevil@gmail.com>
 * @since 0.1.0
 */
class Nav extends \yii\bootstrap\Nav
{
    /**
     * @inheritdoc
     */
    protected function isItemActive($item)
    {
        if (isset($item['url']) && $item['url'] === Yii::$app->request->absoluteUrl) {
           
            return true;
        }
        return false;
    }
}
