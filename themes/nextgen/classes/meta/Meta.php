<?php
/**
 * @link http://www.nextgen.com/
 * @copyright Copyright (c) 2015 nextgen
 * @license http://www.nextgen.com/license/
 */

namespace themes\nextgen\classes\meta;

use Yii;
use yii\base\BaseObject;

/**
 * Class MetaBox
 *
 * @author Agiel K. Saputra <13nightevil@gmail.com>
 * @since 0.1.0
 */
class Meta extends BaseObject
{
    /**
     * @var \common\models\Post
     */
    public $model;

    /**
     * @var \yii\widgets\ActiveForm
     */
    public $form;

    /**
     * @inheritdoc
     */
    public function init()
    {
        $this->renderBox();
    }

    public function renderBox()
    {
        echo Yii::$app->view->renderFile(__DIR__ . '/views/_form.php', [
            'model' => $this->model,
            'form'  => $this->form
        ]);
    }
}
